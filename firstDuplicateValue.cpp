/*Problem:
Given an array of integers between 1 and n,inclusive where n is the length of the array,
write a function that returns the first integer that appears more than once(when array is read from left to right)
In other words,out of all the integers that might occur more than once in the input array,your function
should return the one whose first duplicate value has the minimum index.
If no integer appears more than once,your function should return -1.
Note that you are allowed to mutate the input array.*/

//Solution:
//Time Complexity:O(N) , Space Complexity:O(N)
#include<iostream>
#include<vector>
using namespace std;


int firstDuplicateValue(vector<int> array) { 
  unordered_set <int> uset;
  for(auto i:array){
    if(uset.find(i)!=uset.end())
      return i;
    uset.insert(i);
  }
  
  return -1;

}
int main(){
	vector<int>A={2,1,5,2,3,3,4};
	cout<<firstDuplicateValue(A);
	return 0;
}



//Time Complexity:O(N) , Space Complexity:O(1)
#include<iostream>
#include<vector>
using namespace std;
int firstDuplicateValue(vector<int> array) { 
  for(int num:array){
    int index=abs(num)-1;
    if(array[index]<0)
      return abs(num);
    else
      array[index]=-1*array[index];
    
  }
  
  return -1; 
}
int main(){
	vector<int>A={2,1,5,2,3,3,4};
	cout<<firstDuplicateValue(A);
	return 0;
}
