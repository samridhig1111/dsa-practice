//Remove Duplicates from sorted LinkedList
#include<iostream>
using namespace std;


class LinkedList {
public:
    int value;
    LinkedList *next = nullptr;

    LinkedList(int value) { this->value = value; }
};

LinkedList *removeDuplicatesFromLinkedList(LinkedList *linkedList) {
    
    LinkedList *root = linkedList;
  
    while(linkedList -> next != NULL){
      
    if(linkedList -> next -> value == linkedList -> value){
      
     //update the pointer of next node
        LinkedList *node = linkedList -> next;
        linkedList -> next = linkedList -> next -> next;
        delete node;
        continue;
    
    }
  
    //move to next node
    linkedList = linkedList -> next;
}

    return root;
}
void insert(LinkedList*& root ,int val){
    LinkedList* l = new LinkedList(val);
    l -> next = root;
    root = l;
}
int main(){
    LinkedList* root = NULL;
    insert(root, 6);
    insert(root, 6);
    insert(root, 5);
    insert(root, 5);
    insert(root, 4);
    insert(root, 3);
    insert(root, 1);
    root = removeDuplicatesFromLinkedList(root);
    while(root != NULL){
        cout<<root -> value;
        root = root -> next;
    }
    return 0;
}
