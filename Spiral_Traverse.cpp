Problem:
/*Write a function that takes in an n x m two-dimensional array(that can be square-shaped when n==m) and returns a one-dimenional
array of all the array elements in spiral order.
Spiral Order starts at the top left corner of the two-dimensional array,goes to the right,and proceeds in a spiral pattern all the way until 
every element has been visited*/


Solution:
using namespace std;

vector<int> spiralTraverse(vector<vector<int>> array) {
  

  int leftCol=0;
  int rightCol=array[0].size()-1;
  int top=0;
  int bottom=array.size()-1;
  vector<int>ans;
  while(leftCol<=rightCol && top<=bottom){
    for(int c=leftCol;c<=rightCol;c++){
      ans.push_back(array[top][c]);
    }
    top++;
    if(top>bottom){
      break;
    }
    for(int r=top;r<=bottom;r++){
      ans.push_back(array[r][rightCol]);
    }
    rightCol--;
    if(rightCol<leftCol)
      break;
    for(int c=rightCol;c>=leftCol;c--){
      ans.push_back(array[bottom][c]);
    }
    bottom--;
    if(bottom<top){
      break;
    }
    for(int r=bottom;r>=top;r--){
      ans.push_back(array[r][leftCol]);
    }
    leftCol++;
   if(leftCol>rightCol)
     break;
  }
  return ans;
}
