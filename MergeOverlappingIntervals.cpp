/*Write a function that takes in a non-empty array of arbitrary intervals,merges any overlapping intervals
and returns the new intervals in no particular order.
Each interval is an array of two integers,with interval[0] as the start of the interval and interval[1]
as the end of the interval.
Example : [1,5] and [6,7] are not overlapping.
[1,6] and [6,7] are overlapping*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

//Time Complexity:O(nlog(n)) , Space Complexity:O(n)

vector<vector<int>> mergeOverlappingIntervals(vector<vector<int>> intervals) {

  /*First sort the intervals with respect to their starting values.This will allow you to merge 
    all overlapping intervals in a single traversal through the sorted intervals.*/

    vector <vector<int>> output;
    sort(intervals.begin(), intervals.end());
    output.push_back(intervals[0]);

    for(int i=1;i<intervals.size();i++){

      /*At each iteration compare the start of the next interval to the end of the current interval 
        to look for an overlap.If you find an overlap,change the end of the current interval with the 
        max(previous_end,current_end)*/

        int currentStart = intervals[i][0];
        int currentEnd = intervals[i][1];

        int previousStart=output.back()[0];
        int previousEnd=output.back()[1];

        if(previousEnd>=currentStart)

            output.back()[1]=max(previousEnd,currentEnd);
        
        else

            output.push_back(intervals[i]); 

    }

  return output;
}

int main()

{
    vector<vector<int>> intervals = {{1,2}, {3,5}, {4,7}, {6,8}, {9,10}};

    int rows = intervals.size();
    int cols=intervals[0].size();

    if(rows <= 0 || cols <= 0){
        cout << "Invalid input";
        return 0;
    }

    vector<vector<int>> output = mergeOverlappingIntervals(intervals);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++)
            cout << output[i][j] << " ";
        cout << endl;        
    }
    return 0;
}
	
