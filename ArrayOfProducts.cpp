/*Problem:
Write a function that takes in a non-empty array of integers and returns an array of the same length,
where eachelement in the output array is equal to the product of every other number in the input array.
In other words,the value at output[i] is equal to the product of every number in the input array other than input[i].*/

//Solution:

#include<iostream>
#include<vector>
using namespace std;

vector<int> arrayOfProducts(vector<int>array){
	vector<int>output;
	output.push_back(1);
	for(int i=1;i<array.size();i++){
		output.push_back(array[i-1]*output[i-1]);
	}
	int value=array[array.size()-1];
	for(int i=array.size()-2;i>=0;i--){
		output[i]=output[i]*value;
		value=value*array[i];
	}
return output;
}
int main(){
	vector<int>A={1,2,3,3,4,0,10,6,5,-1,-3,2,3};
	cout<<arrayOfProducts(A);
	return 0;
}