/*Write a function that takes in a binary search tree and a traget integer value and returns the closest value to that
target value contained in the BST.You can assume that there will only be one closest value*/


#include <iostream>


using namespace std;


struct Node {

	int value;
	Node * left;
	Node * right;

	Node(int n) : value(n), left(NULL), right(NULL) {}

};



int is_bst(Node * tree, int target){

	int closest = tree->value;

      while(tree != NULL){

        if(abs(target - closest) > abs(target - tree->value))
          closest = tree->value;

        if(target < tree->value)
          tree = tree->left;
        else
          tree = tree->right;

  }

  return closest;
}



int main(){


    Node *tree = new Node(5);
    tree->left = new Node(3);
    tree->right = new Node(6);
    tree->left->right = new Node(4);
    tree->left->left = new Node(2);
    tree->right->right = new Node(8);

    int target = 1;

	cout << is_bst(tree, target) << endl;

	return 0;

}
