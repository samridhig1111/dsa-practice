/* BST Construction
Write a BST class for a binary search tree. The class should support:
1. Inserting values with the insert method.
2. Removing values with the remove method; this method should only remove the first instance of a given value.
3. Searching for values with the contains method
Note that you can't remove values from a single-node tree. In other words, calling the remove method on a single
-node tree should simply not do anything.
*/

using namespace std;
class BST{
  private:
    BST *InsertNode(BST *node, int val){
      if(!node)
        return new BST(val);
      if(val < node -> value)
        node -> left = InsertNode(node -> left, val);
      else
        node -> right = InsertNode(node -> right, val);
      return node;
    }


    BST *successor(BST *node){
      if(!node -> right)
        return node;
      node = node -> right;
      while(node -> left != NULL)
        node = node -> left;
      return node;
    }


    BST *RemoveNode(BST *node, int val){
      if(val < node -> value)
        node -> left = RemoveNode(node -> left, val);
      else if(val > node -> value)
        node -> right = RemoveNode(node -> right, val);
      else{
        if(!node -> left && !node -> right){
          delete node;
          return NULL;
        }
        else if(!node -> left || !node -> right){
          BST *n = node -> left ? node -> left : node -> right;
          delete node;
          return n;
        }
        else{
          BST *s = successor(node);
          node -> value = s -> value;
          node ->right = RemoveNode(node -> right, s -> value);
        }
      }
      return node;
    }


public:
  int value;
  BST *left;
  BST *right;
  BST(int val){
    value = val;
    left = NULL;
    right = NULL;
  }

  BST &insert(int val){
    InsertNode(this, val);
    return *this;
  }

  bool contains(int val){
    BST *root = this;
    while(root != NULL){
      if(val == root -> value)
        return true;
      else if(val < root -> value)
        root = root -> left;
      else 
        root = root -> right;
    }
    return false;
  }

  BST &remove(int val){
    if((!this -> left && !this -> right) || !contains(val))
      return *this;
    if(this -> value == val && (!this -> left || !this -> right)){
      BST **n = this -> left ? &this -> left : &this -> right;
      this -> value = (*n) -> value;
      *n = RemoveNode(*n, (*n) -> value);
    }
    else
      RemoveNode(this, val);
    return *this;
  }
};
