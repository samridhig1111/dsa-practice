/*Problem:
Write a function that takes in an array of integers and returns the length of the longest peak in the array.
A peak is defined as adjacent integers in the array that are strictly increasing until they reach a tip 
(highest value in the peak),at which point they become strictly decreasing.At least three integers are required to form a peak.
For example the integers 1,4,10,2 form a peak , but the integers 4,0,10 don't and neither do the integers 1,2,2,0.
Similarly the integers 1,2,3 don't form a peak because there aren't any strictly decreasing integers after the 3*/

// Solution:

#include<iostream>
#include<vector>
using namespace std;

int longestPeak(vector<int> array) {
  // Write your code here.
  bool increase=false,decrease=false;
  int cont=1,maxcont=0;
  for(int i=1;i<array.size();i++){
    if(array[i-1]<array[i] && decrease==false){
      increase=true;
      cont++;
    }
    else if(array[i-1]>array[i] && increase==true){
        decrease=true;
      cont++;
    }
    else{
      decrease=false;
      increase=false;
      cont=1;
      if(array[i-1]<array[i])
      {
        increase=true;
        cont++;
      }
    }
    if(increase==true && decrease==true){
      maxcont=max(maxcont,cont);
    }
  }
  return maxcont;
}
int main(){
	vector<int>A={1,2,3,3,4,0,10,6,5,-1,-3,2,3};
	cout<<longestPeak(A);
	return 0;
}