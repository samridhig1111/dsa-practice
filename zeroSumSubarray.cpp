/*You are given a list of integers nums.Write a function that returns a boolean representing whether there exists
a zero-sum subarray of nums. A zero sum-subarray is any subarray where all of the values add up to zero.A subarray
is any contiguous section of the array. For the purposes of this problem,a subarray can be as small as one element
and as long as the original array*/

#include<iostream>
#include<vector>
#include<unordered_set>

using namespace std;


bool zeroSumSubarray(vector<int> nums) {
    /* We are adding all the values in nums and storing it in unordered set until the sum is zero or the sum is already 

    present in the unordered set. If the sum is already present in unordered set that means in between that 

    the subarrays sum was zero*/

    if(nums.size() == 0)
        return false;
    
    if(nums.size() == 1)
        return nums[0]==0;
    

    auto it = nums.begin();
    unordered_set<int> records;

    while(it != nums.end()){
        
        *it += *(it-1);

        if(*it == 0 || !records.insert(*it).second)
            return true;
        
        ++it;
    }

    return false;
}
int main()

{
    vector<int> nums = {-5, -5, 2, 3, -2};
    
    zeroSumSubarray(nums) ? cout << "true" : cout << "false";
    
    return 0;
}
