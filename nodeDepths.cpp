/*The distance between a node in a Binary Tree and the tree's root is called the node's depth.Write
a function that takes in a Binary Tree and returns the sum of its nodes depths.*/

#include<iostream>

using namespace std;

class Node {
public:
  int value;
  Node *left;
  Node *right;

  Node(int value) {
    this->value = value;
    left = NULL;
    right = NULL;
  }
};

int sumOfDepthsRecur(int depth,Node *node){
    if (!node) {
        return 0;
    }
  return depth + sumOfDepthsRecur(depth + 1, node->left) + sumOfDepthsRecur(depth + 1, node->right);
}

int sumOfDepths(Node *root) {
  return sumOfDepthsRecur(0, root);
}

int main() {

    Node *tree = new Node(5);
    tree->left = new Node(3);
    tree->right = new Node(6);
    tree->left->right = new Node(4);
    tree->left->left = new Node(2);
    tree->right->right = new Node(8);
    
    cout << sumOfDepths(tree);
    
    return 0;
}
