/* Write a function that takes in a binary tree and returns a list of its branch sums ordered from leftmost branch sum to 
rightmost branch sum.
A branch sum is the sum of all values in a binary tree branch.A binary tree branch is a path of nodes in a tree that 
starts at the roor node and ends at any leaf node.*/

#include<vector>
#include<iostream>

using namespace std;

class BinaryTree {

public:
  int value;
  BinaryTree *left;
  BinaryTree *right;

  BinaryTree(int value) {
    this->value = value;
    left = NULL;
    right = NULL;
  }
};

void Helper(BinaryTree*,int,vector<int>&);

vector<int> branchSums(BinaryTree *root) {
  vector<int> branchSums = {};
  Helper(root,0,branchSums);
  return branchSums;
}

void Helper(BinaryTree* node, int branchTotal,vector<int> &branchSums){
  branchTotal += node->value;
  if(node->left == NULL && node->right == NULL){
      branchSums.push_back(branchTotal);
      return;
  }
  else{
    if(node->left != NULL){
        Helper(node->left, branchTotal, branchSums);
    }
    if(node->right != NULL){
        Helper(node->right, branchTotal, branchSums);
    }
  }
}

int main(){

    BinaryTree *tree = new BinaryTree(5);
    tree->left = new BinaryTree(3);
    tree->right = new BinaryTree(6);
    tree->left->right = new BinaryTree(4);
    tree->left->left = new BinaryTree(2);
    tree->right->right = new BinaryTree(8);
    vector<int> output = branchSums(tree);

    for(int i=0;i<output.size();i++){
        cout<<output[i]<<" ";
    }

    return 0;
}